<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LandingController;
use App\Http\Controllers\Admin\WebController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\Admin\MainController;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\ProdukController;
use App\Http\Controllers\Admin\AnggotaController;
use App\Http\Controllers\Admin\KaryawanController;
use App\Http\Controllers\Admin\KoperasiController;
use App\Http\Controllers\ErrorController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/cek', function(){
    return view('welcome');
});

Route::get('/', [LandingController::class, 'show']);
Route::get('/Grup', [LandingController::class, 'grup'])->name('grup');

Route::get('/Dashboard', [DashboardController::class, 'show']);

//admin page
Route::get('/AdminPage', [MainController::class, 'dashboard'])->name('AdminPage');
Route::get('/Login', [MainController::class, 'login']);

Route::get('/AdminPage/Website', [KoperasiController::class, 'website'])->name('Website');
Route::post('/AdminPage/Website', [KoperasiController::class, 'update'])->name('editWebsite');


Route::get('/AdminPage/Produk', [ProdukController::class, 'produk'])->name('Produk');
Route::get('/AdminPage/Produk/TambahProduk', [ProdukController::class, 'tambah'])->name('TambahProduk');
Route::post('/AdminPage/Produk/TambahProduk', [ProdukController::class, 'insertproduk'])->name('InsertProduk');
Route::get('/AdminPage/EditProduk/{id}', [ProdukController::class, 'editproduk'])->name('editProduk');
Route::post('/AdminPage/EditProduk/{id}', [ProdukController::class, 'updateproduk'])->name('UpdateProduk');
Route::get('/AdminPage/DeleteProduk/{id}', [ProdukController::class, 'destroyproduk'])->name('DestroyProduk');

Route::get('/AdminPage/Produk/TambahKategoriProduk', [ProdukController::class, 'tambahkategori'])->name('TambahKategoriProduk');
Route::post('/AdminPage/Produk/TambahKategoriProduk', [ProdukController::class, 'insertkategori'])->name('InsertKategoriProduk');
Route::get('/AdminPage/EditKategoriProduk/{id}', [ProdukController::class, 'editkategori'])->name('editKategoriProduk');
Route::post('/AdminPage/EditKategoriProduk/{id}', [ProdukController::class, 'updatekategori'])->name('UpdateKategoriProduk');
Route::get('/AdminPage/DeleteKategoriProduk/{id}', [ProdukController::class, 'destroykategori'])->name('DestroyKategoriProduk');

Route::get('/AdminPage/DataWebsite', [WebController::class, 'dataweb'])->name('DataWebsite');
Route::get('/AdminPage/DataWebsite/TambahData', [WebController::class, 'tambah'])->name('TambahDataWebsite');
Route::post('/AdminPage/DataWebsite/TambahData', [WebController::class, 'create'])->name('InsertDataWebsite');
Route::get('/AdminPage/DataWebsite/TambahKegiatan', [WebController::class, 'tambahkegiatan'])->name('TambahDataKegiatan');

Route::get('/AdminPage/DataAdmin', [AdminController::class, 'dataadmin'])->name('DataAdmin');
Route::get('/AdminPage/DataAdmin/TambahData', [AdminController::class, 'tambah'])->name('TambahDataAdmin');
Route::get('/AdminPage/DataAdmin/TambahStatus', [AdminController::class, 'tambahstatus'])->name('TambahDataStatus');
Route::post('/AdminPage/DataAdmin/TambahStatus', [AdminController::class, 'insertstatus'])->name('InsertDataStatus');

Route::get('/AdminPage/DataKaryawan', [KaryawanController::class, 'datakaryawan'])->name('DataKaryawan');
Route::get('/AdminPage/DataKaryawan/TambahData', [KaryawanController::class, 'tambah'])->name('TambahDataKaryawan');

Route::get('/AdminPage/Anggota', [AnggotaController::class, 'dataanggota'])->name('DataAnggota');
Route::get('/AdminPage/Anggota/TambahData', [AnggotaController::class, 'tambah'])->name('TambahDataAnggota');


//Error
Route::get('/Error', [ErrorController::class, 'error'])->name('error');