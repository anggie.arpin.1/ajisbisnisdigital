<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKoperasiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('koperasi', function (Blueprint $table) {
            $table->bigIncrements('id_koperasi');
            $table->string('nama_koperasi');
            $table->string('visi');
            $table->text('misi');
            $table->string('manfaat');
            $table->string('link_facebook');
            $table->string('link_instagram');
            $table->string('link_whatsapp');
            $table->string('ad_art');
            $table->string('syarat_keanggotaan');
            $table->string('struktur_pengurus');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('koperasi');
    }
}
