<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShareProdukTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('share_produk', function (Blueprint $table) {
            $table->bigIncrements('id_share_product');
            $table->bigInteger('id_produk');
            $table->string('kd_karyawan');
            $table->string('link');
            $table->bigInteger('jml_akses');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('share_produk');
    }
}
