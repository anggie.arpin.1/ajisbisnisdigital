<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailDokumenKegiatanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_dokumen_kegiatan', function (Blueprint $table) {
            $table->bigIncrements('id_detail_kegiatan');
            $table->bigInteger('id_kegiatan');
            $table->bigInteger('id_koperasi');
            $table->string('foto_dokumentasi');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_dokumen_kegiatan');
    }
}
