<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\mStatus;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function dataadmin()
    {
        $status = mStatus::all();
        return view('Admin.Content.DataAdmin.DataAdmin', ['status' => $status]);
    }

    public function tambah()
    {
        $status = mStatus::all();
        return view('Admin.Content.DataAdmin.FormTambahAdmin',['status' => $status]);
    }

    public function tambahstatus()
    {
        return view('Admin.Content.DataAdmin.FormTambahStatus');
    }

    public function insertstatus(Request $request)
    {
        $data = mStatus::create([
            'nama_status' => $request->nama_status
        ]);
        return redirect('/AdminPage/DataAdmin');
    }
}
