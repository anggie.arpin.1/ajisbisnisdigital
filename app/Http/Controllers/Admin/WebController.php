<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\mDokumenKegiatan;
use App\Models\mKoperasi;
use App\Models\mShare;
use Illuminate\Http\Request;

class WebController extends Controller
{
    public function dataweb()
    {
        $data = mKoperasi::paginate(5);
        $kegiatan = mDokumenKegiatan::paginate(15);
        return view('Admin.Content.DataWeb.DataWeb',['data'=>$data, 'kegiatan' => $kegiatan]);
    }

    public function tambah()
    {
        return view('Admin.Content.DataWeb.FormTambahWeb');
    }

    
    public function create(Request $request)
    {
        $data = mKoperasi::create([
            'nama_koperasi' => $request->nama,
            'visi' => $request->visi,
            'misi' => $request->misi,
            'manfaat' => $request->manfaat, 
            'link_facebook' => $request->facebook,
            'link_instagram' => $request->instagram,
            'link_whatsapp' => $request->whatsapp,
            'ad_art' => 'null',
            'syarat_keanggotaan' => 'null',
            'struktur_pengurus' => 'null'
        ]);
        return redirect('/AdminPage/DataWebsite');
    }

    public function tambahkegiatan()
    {
        return view('Admin.Content.DataWeb.FormTambahKegiatan');
    }
}
