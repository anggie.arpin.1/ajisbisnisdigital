<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\mKoperasi;
use Illuminate\Http\Request;

class KaryawanController extends Controller
{
    public function datakaryawan()
    {
        return view('Admin.Content.DataKaryawan.DataKaryawan');
    }

    public function tambah()
    {
        $koperasi = mKoperasi::all();
        return view('Admin.Content.DataKaryawan.FormTambahKaryawan', ['koperasi' => $koperasi]);
    }
}
