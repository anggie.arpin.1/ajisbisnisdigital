<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\mKoperasi;
use Illuminate\Http\Request;
use Prophecy\Doubler\Generator\Node\ReturnTypeNode;

class KoperasiController extends Controller
{
    public function website($id)
    {
        $koperasi = mKoperasi::where('id_koperasi',$id)->first();
        return view('Admin.Content.DataKoperasi.Koperasi',['koperasi' => $koperasi]);
    }
}    
