<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AnggotaController extends Controller
{
    public function dataanggota()
    {
        return view('Admin.Content.DataAnggota.DataAnggota');
    }

    public function tambah()
    {
        return view('Admin.Content.DataAnggota.FormTambahAnggota');
    }
}
