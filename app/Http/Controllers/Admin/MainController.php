<?php

namespace App\Http\Controllers\Admin;

use App\Models\mAkun;
use App\Models\mProduk;
use App\Models\mKoperasi;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MainController extends Controller
{
    public function Dashboard()
    {
        $web = mKoperasi::count();
        $anggota = mAkun::where('id_status','=','5')->count();
        $produk = mProduk::count();
        $admin = mAkun::where('id_status', '=', '2')->count();
        return view('Admin.Content.Dashboard', ['web'=>$web, 'anggota' =>$anggota, 'produk' => $produk, 'admin' => $admin]);
    
    }

    public function login()
    {
        return view('Auth.login');
    }
}
