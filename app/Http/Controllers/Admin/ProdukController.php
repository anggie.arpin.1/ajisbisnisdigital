<?php

namespace App\Http\Controllers\Admin;

use App\Models\mShare;
use App\Models\mProduk;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\mKategoriProduk;

class ProdukController extends Controller
{
    public function produk()
    {
        $data = mProduk::join('kategori_produk', 'kategori_produk.id_kategori_produk', '=', 'produk.id_kategori_produk')->paginate(10);
        $kategori = mKategoriProduk::paginate(10);
        $link = mShare::paginate(10);
        return view('Admin.Content.DataProduk.Produk',['data' => $data, 'kategori' =>$kategori, 'link' => $link]);
    }

    public function tambah()
    {
        $kategori = mKategoriProduk::all();
        return view('Admin.Content.DataProduk.FormTambahProduk', ['kategori' => $kategori]);
    }

    public function tambahkategori()
    {
        return view('Admin.Content.DataProduk.FormTambahKategoriProduk');
    }

    public function insertproduk(Request $request)
    {
        $data = mProduk::create([
            'nama_produk' => $request->nama_produk,
            'harga' => $request->harga,
            'deskripsi' => $request->deskripsi,
            'qty' => $request->jumlah,
            'id_kategori_produk' => $request->kategoriproduk
        ]);
        return redirect('/AdminPage/Produk');
    }

    public function editproduk($id)
    {
        $kategori = mKategoriProduk::all();
        $produk = mProduk::where('id_kategori_produk', $id)->first();
        return view('Admin.Content.DataProduk.FormEditProduk', ['produk'=>$produk, 'kategori'=>$kategori]);
    }

    public function updateproduk(Request $request, Int $id)
    {
        $data = mProduk::where('id_produk', $id)->update(
            [
                'nama_produk' => $request->nama_produk,
                'harga' => $request->harga,
                'deskripsi' => $request->deskripsi,
                'qty' => $request->jumlah,
                'id_kategori_produk' => $request->kategoriproduk
            ]
        );

        return redirect('/AdminPage/Produk');
    }

    public function destroyproduk($id)
    {
        mProduk::where('id_produk',$id)->delete();
        return redirect('/AdminPage/Produk');
    }

    public function insertkategori(Request $request)
    {
        $data = mKategoriProduk::create([
            'nama_kategori' => $request->nama_kategori
        ]);
        return redirect('/AdminPage/Produk');
    }

    public function editkategori($id)
    {
        $kategori = mKategoriProduk::where('id_kategori_produk', $id)->first();
        return view('Admin.Content.DataProduk.FormEditKategoriProduk', ['kategori'=>$kategori]);
    }

    public function updatekategori(Request $request, Int $id)
    {
        $data = mKategoriProduk::where('id_kategori_produk', $id)->update(
            [
                'nama_kategori' => $request->nama_kategori
            ]
        );
        return redirect('/AdminPage/Produk');
    }

    public function destroykategori($id)
    {
        $kategori = mProduk::where('id_kategori_produk',$id)->first();
        if($kategori){
            return redirect('/AdminPage/Produk');
        }else{
            mKategoriProduk::where('id_kategori_produk',$id)->delete();
            return redirect('/AdminPage/Produk');
        }
    }
}
