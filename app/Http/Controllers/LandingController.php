<?php

namespace App\Http\Controllers;

use App\Models\mKoperasi;
use Illuminate\Http\Request;

class LandingController extends Controller
{
    public function show()
    {
        $data = mKoperasi::all();
        return view('Landing.Layout.LandingLayout',['data' => $data]);
    }

    public function grup()
    {
        $data = mKoperasi::all();
        return view('Landing.Content.Grup',['data' => $data]);
    }
}
