<?php

namespace App\Http\Controllers;

use App\Models\mAkun;
use App\Models\mKoperasi;
use App\Models\mProduk;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function show()
    {
        return view('Landing.Content.DKUM');
    }
}
