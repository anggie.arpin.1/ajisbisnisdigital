<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class mKoperasi extends Model
{
    protected $table = 'koperasi';
    protected $primaryKey = 'id_koperasi';
    protected $fillable =[
        'nama_koperasi',
        'visi',
        'misi',
        'manfaat',
        'link_facebook',
        'link_instagram',
        'link_whatsapp',
        'ad_art',
        'syarat_keanggotaan',
        'struktur_pengurus'
    ];
    // use HasFactory;
}
