<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class mProduk extends Model
{
    protected $table = 'produk';
    protected $primaryKey = 'id_produk';
    protected $fillable =[
        'nama_produk',
        'harga',
        'deskripsi',
        'qty',
        'id_kategori_produk',
    ];
    // use HasFactory;

    function kategori()
    {
        return $this->belongsTo(mKategoriProduk::class, 'id_kategori_produk');
    }
}
