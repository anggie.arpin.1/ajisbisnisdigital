<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class mAkun extends Model
{
    protected $table = 'akun';
    protected $primaryKey = 'id_akun';
    protected $fillable =[
        'email',
        'password',
        'nama',
        'nomor_hp',
        'alamat',
        'foto_profil',
        'id_status'
    ];
    // use HasFactory;
}
