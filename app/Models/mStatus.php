<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class mStatus extends Model
{
    protected $table = 'status';
    protected $primaryKey = 'id_status';
    protected $fillable =[
        'nama_status',
    ];
    // use HasFactory;
}
