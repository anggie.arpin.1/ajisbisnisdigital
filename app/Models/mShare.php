<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class mShare extends Model
{
    protected $table = 'share_produk';
    protected $primaryKey = 'id_share_produk';
    protected $fillable =[
        'id_produk',
        'kd_karyawan',
        'link',
        'jml_akses'
    ];
    // use HasFactory;
}
