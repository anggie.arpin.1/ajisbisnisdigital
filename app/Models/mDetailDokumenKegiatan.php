<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class mDetailDokumenKegiatan extends Model
{
    protected $table = 'detail_dokumen_kegiatan';
    protected $primaryKey = 'id_detail_kegiatan';
    protected $fillable =[
        'id_kegiatan',
        'id_koperasi',
        'foto_dokumentasi'
    ];
    // use HasFactory;
}
