<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class mDokumenKegiatan extends Model
{
    protected $table = 'dokumen_kegiatan';
    protected $primaryKey = 'id_kegiatan';
    protected $fillable =[
        'nama_kegiatan',
        'tempat_kegiatan',
        'tgl_kegiatan',
        'deskripsi_kegiatan',
        'file_dokumen'
    ];
    // use HasFactory;
}
