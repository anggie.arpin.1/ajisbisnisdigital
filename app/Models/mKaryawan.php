<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class mKaryawan extends Model
{
    protected $table = 'karyawan';
    protected $primaryKey = 'kd_karyawan';
    protected $fillable =[
        'Id_akun',
        'no_ktp',
        'nama_akun_fb',
        'id_koperasi'
    ];
    // use HasFactory;
}
