<!DOCTYPE html>
<html lang="en">

<head>

	<title>Halaman Tidak Ditemukan</title>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

	<!-- Bootstrap -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">

	<!-- Custom stlylesheet -->
	<link type="text/css" rel="stylesheet" href="css/style.css" />

</head>

<body>

	<div class="vertical-center align-middle">
		<div class="container mt-4">
			<div id="notfound" class="text-center ">
				<div class="mt-4 mb-4">
                    <img src="https://cdn-icons.flaticon.com/png/512/2814/premium/2814650.png?token=exp=1639125747~hmac=665f6b9aae5a0382e53ad96d63889399" alt="404" width="200px">
				    <h1>Oppsss...</h1>
                </div>
                <div class="mt-4">
                    <h4>Halaman Sedang Diperbaiki</h4>
                    <p>Mohon maaf atas ketidaknyamanannya, halaman akan segera dapat diakses.</p>
                    <a href="{{route('error')}}" class="btn btn-primary">Kembali ke Halaman Utama</a>
                </div>
			</div>
		</div>
	</div>

</body>

</html>