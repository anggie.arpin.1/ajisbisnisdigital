<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login admin</title>
    <link href="{{asset('favicon.ico')}}" rel="shortcut icon"  type="image/x-icon">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="{{asset('admin/dist/css/auth.css')}}">
</head>
<body onload="myFunction()" style="margin:0;" class="my-login-page" >
    <div id="loader"></div>

    <div style="display:none;" id="myDiv" class="animate-bottom">
        <section class="h-100">
                <div class="container h-100">
                    <div class="row justify-content-md-center h-100">
                        <div class="card-wrapper">
                            <div class="brand">
                                <img src="{{asset('admin/dist/img/koperasi.png')}}" alt="logo">
                            </div>
                            <div class="card fat">
                                <div class="card-body">
                                    <h4 class="card-title text-center">Login</h4>
                                    <form action="" method="post" class="my-login-validation">
                                        <div class="form-group text-start">
                                            <label for="Email">Email</label>
                                            <input id="email" type="email" class="form-control" placeholder="Ajis@ajis.com" name="email" value="" required autofocus>
                                            {{-- <div class="invalid-feedback">
                                                Email masih kosong
                                            </div> --}}
                                        </div>

                                        <div class="form-group text-start">
                                            <label for="password">Password</label>
                                            <input id="password" type="password" class="form-control" placeholder="*********" name="password" required data-eye>
                                            {{-- <div class="invalid-feedback">
                                                password masih kosong
                                            </div> --}}
                                        </div>

                                        <div class="d-flex justify-content-center mb-4">   
                                            <a href="/" class="float-md-none">
                                                    Forgot Password?
                                                </a>
                                        </div>

                                        <div class="form-group mb-2">   
                                            {{-- <input type="submit" value="Login" class="btn btn-primary form-control"> --}}
                                        </div>
                                    </form>
                                    <a href="{{route('AdminPage')}}" class="btn btn-primary form-control">Login</a>
                                </div>
                            </div>
                            <div class="footer">
                                Copyright  &copy; 2021 - Aji's Bisnis Digital
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>

    <script>
    var myVar;
        function myFunction() {
        myVar = setTimeout(showPage, 2000);
        }

        function showPage() {
        document.getElementById("loader").style.display = "none";
        document.getElementById("myDiv").style.display = "block";
    }
    </script>

	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	<script src="{{asset('admin/dist/js/my-login.js')}}"></script>
</body>
</html>