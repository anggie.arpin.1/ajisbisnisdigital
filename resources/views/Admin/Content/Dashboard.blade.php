@extends('Admin.Layout.Main')

@section('head')
    Dashboard
@endsection

@section('sidebar')
    <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="" class="brand-link">
      <img src="{{asset('assets/images/koperasi.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">Admin</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item menu-open">
            <a href="{{route('AdminPage')}}" class="nav-link active">
              <i class="nav-icon fas fa-home"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-globe"></i>
              <p>
                Website
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('Website')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Koperasi DKUM</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="{{route('Produk')}}" class="nav-link">
              <i class="nav-icon fas fa-boxes"></i>
              <p>
                Produk
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-database"></i>
              <p>
                Master Data
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('DataWebsite')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Data Website</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('DataAdmin')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Data Admin</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('DataKaryawan')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Data Karyawan</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('DataAnggota')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Data Anggota</p>
                </a>
              </li>
            </ul>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
@endsection

@section('content')
  <div class="jumbotron jumbotron-fluid mt-2 p-4">
    <div class="container">
      <h2 class="mt-2">Selamat Datang <b>Super Admin</b></h2>
      <p class="lead">Pastikan Email dan Password anda terjaga kerahasiaannya.</p>
    </div>
  </div>
  <!-- Small Box (Stat card) -->
  <div class="row">
    <div class="col-lg-3 col-6">
      <!-- small card -->
      <div class="small-box bg-info">
        <div class="inner">
          <h3>{{$web}}</h3>

          <p>Website</p>
        </div>
        <div class="icon">
          <i class="fas fa-globe"></i>
        </div>
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-6">
      <!-- small card -->
      <div class="small-box bg-success">
        <div class="inner">
          <h3>{{$anggota}}</h3>

          <p>Anggota</p>
        </div>
        <div class="icon">
          <i class="fas fa-users"></i>
        </div>
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-6">
      <!-- small card -->
      <div class="small-box bg-warning">
        <div class="inner">
          <h3>{{$produk}}</h3>

          <p>Produk</p>
        </div>
        <div class="icon">
          <i class="fas fa-box"></i>
        </div>
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-6">
      <!-- small card -->
      <div class="small-box bg-danger">
        <div class="inner">
          <h3>{{$admin}}</h3>

          <p>Admin</p>
        </div>
        <div class="icon">
          <i class="fas fa-user-shield"></i>
        </div>
      </div>
    </div>
    <!-- ./col -->
  </div>
  <!-- /.row -->

  <div class="row">
    <div class="col-12">
      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Informasi Akun</h3>
        </div>
        <div class="card-body">
          Berikut adalah data informasi akun anda :
          <ul>
            <li>Kode Karyawan : <b>Kar098BLL</b></li>
            <li>Nama : <b>Putu Indah Permata Aji</b></li>
            <li>Nomor Handphone : <b>+62 831-0290-0652</b></li>
            <li>Alamat : <b>Banjar Kanginan, Desa Julah</b></li>
            <li>Tempat Bertugas : <b>Koperasi Duta Kriya Usaha Mandiri</b></li>
            <li>Jabatan : <b>Manager Simpan Pinjam</b></li>
          </ul>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </div>
  </div>
@endsection