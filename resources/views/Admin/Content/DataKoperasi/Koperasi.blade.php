@extends('Admin.Layout.Main')

@section('css')
  <script src="https://code.jquery.com/jquery-3.5.1.min.js" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>

  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

  <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.css" rel="stylesheet">
  <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.js"></script>
@endsection

@section('head')
    Data Koperasi
@endsection

@section('sidebar')
    <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="" class="brand-link">
      <img src="{{asset('assets/images/koperasi.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">Admin</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item">
            <a href="{{route('AdminPage')}}" class="nav-link">
              <i class="nav-icon fas fa-home"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-globe"></i>
              <p>
                Website
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('Website')}}" class="nav-link ">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Koperasi DKUM</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="{{route('Produk')}}" class="nav-link">
              <i class="nav-icon fas fa-boxes"></i>
              <p>
                Produk
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-database"></i>
              <p>
                Master Data
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('DataWebsite')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Data Website</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('DataAdmin')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Data Admin</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('DataKaryawan')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Data Karyawan</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('DataAnggota')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Data Anggota</p>
                </a>
              </li>
            </ul>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
@endsection

@section('content')
  <section class="content-header mt-2">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h3>Koperasi Duta Kriya Usaha Mandiri</h3>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <div class="card card-secondary">
    <!-- form start -->
    <form action="{{route('editWebsite')}}" method="POST">
      <div class="card-body">
        <div class="form-group">
          <label for="exampleInputEmail1">Nama Koperasi</label>
          <input type="text" class="form-control" name="nama" placeholder="Masukkan nama koperasi">
        </div>
        <div class="form-group">
          <label for="">Visi</label>
          <input type="text" class="form-control" name="visi" placeholder="Visi Koperasi">
        </div>
        <div class="form-group">
          <label for="">Misi</label>
          <textarea name="misi" rows="5" id="summernote" class="form-control"></textarea>
        </div>
        <div class="form-group">
          <label for="">Manfaat</label>
          <textarea name="manfaat" rows="5" id="summernote" class="form-control"></textarea>
        </div>
        <div class="form-group">
          <label for="">Link Facebook</label>
          <input type="text" name="facebook" class="form-control" placeholder="Enter text">
        </div>
        <div class="form-group">
          <label for="">Link Instagram</label>
          <input type="text" name="instagram" class="form-control" placeholder="Enter text">
        </div>
        <div class="form-group">
          <label for="">Link Whatsapp</label>
          <input type="text" name="whatsapp" class="form-control" placeholder="Enter text">
        </div>
        <div class="form-group">
          <label for="">Syarat Keanggotaan</label>
            <div class="custom-file">
              <input type="file" name="syarat_keanggotaan" class="custom-file-input" id="customFile">
              <label class="custom-file-label" for="customFile">Pilih Berkas</label>
            </div>
        </div>
        <div class="form-group">
          <label for="">Struktur Pengurus</label>
          <div class="custom-file">
            <input type="file" name="struktur_pengurus" class="custom-file-input" id="customFile">
            <label class="custom-file-label" for="customFile">Pilih Berkas</label>
          </div>
        </div>
      </div>
      <!-- /.card-body -->

      <div class="card-footer">
        <input type="submit" value="Ubah" class="btn btn-warning">
      </div>
    </form>
  </div>
@endsection

@section('script')

@endsection