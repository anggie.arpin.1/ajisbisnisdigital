@extends('Admin.Layout.Main')

@section('css')
  <link rel="stylesheet" href="{{asset('admin/plugins/summernote/summernote-bs4.min.css')}}">
@endsection

@section('head')
    Data Website
@endsection

@section('sidebar')
    <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="" class="brand-link">
      <img src="{{asset('assets/images/koperasi.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">Admin</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item">
            <a href="{{route('AdminPage')}}" class="nav-link">
              <i class="nav-icon fas fa-home"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link ">
              <i class="nav-icon fas fa-globe"></i>
              <p>
                Website
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('Website')}}" class="nav-link ">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Koperasi DKUM</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="{{route('Produk')}}" class="nav-link">
              <i class="nav-icon fas fa-boxes"></i>
              <p>
                Produk
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-database"></i>
              <p>
                Master Data
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('DataWebsite')}}" class="nav-link active">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Data Website</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('DataAdmin')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Data Admin</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('DataKaryawan')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Data Karyawan</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('DataAnggota')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Data Anggota</p>
                </a>
              </li>
            </ul>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
@endsection

@section('content')
  <section class="content-header mt-2">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h3>Tambah Kegiatan</h3>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <div class="card card-secondary">
    <!-- form start -->
    <form action="" method="POST" enctype="multipart/form-data">
      <div class="card-body">
        <div class="form-group">
          <label for="exampleInputEmail1">Nama Kegiatan</label>
          <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Pelaksana Kegiatan</label>
          <select name="pelaksana" id="" class="form-control">
            <option value="">1</option>
            <option value="">1</option>
            <option value="">1</option>
            <option value="">1</option>
          </select>
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Tempat Kegiatan</label>
          <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Tanggal Kegiatan</label>
          <input type="date" class="form-control" id="exampleInputEmail1" placeholder="Enter text">
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">File Dokumen</label>
            <div class="custom-file">
              <input type="file" class="custom-file-input" id="customFile">
              <label class="custom-file-label" for="customFile">Pilih Berkas</label>
            </div>
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Foto Kegiatan</label>
            <div class="custom-file mb-3">
              <input type="file" class="custom-file-input" id="customFile">
              <label class="custom-file-label" for="customFile">Pilih Berkas</label>
            </div>
            <div class="custom-file mb-3">
              <input type="file" class="custom-file-input" id="customFile">
              <label class="custom-file-label" for="customFile">Pilih Berkas</label>
            </div>
            <div class="custom-file ">
              <input type="file" class="custom-file-input" id="customFile">
              <label class="custom-file-label" for="customFile">Pilih Berkas</label>
            </div>
        </div>
      </div>
      <!-- /.card-body -->

      <div class="card-footer">
        <input type="submit" value="Ubah" class="btn btn-warning">
      </div>
    </form>
  </div>
@endsection