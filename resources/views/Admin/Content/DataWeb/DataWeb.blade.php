@extends('Admin.Layout.Main')

@section('head')
    Data Website
@endsection

@section('sidebar')
    <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="" class="brand-link">
      <img src="{{asset('assets/images/koperasi.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">Admin</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item">
            <a href="{{route('AdminPage')}}" class="nav-link">
              <i class="nav-icon fas fa-home"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-globe"></i>
              <p>
                Website
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('Website')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Koperasi DKUM</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="{{route('Produk')}}" class="nav-link">
              <i class="nav-icon fas fa-boxes"></i>
              <p>
                Produk
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-database"></i>
              <p>
                Master Data
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('DataWebsite')}}" class="nav-link active">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Data Website</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('DataAdmin')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Data Admin</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('DataKaryawan')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Data Karyawan</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('DataAnggota')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Data Anggota</p>
                </a>
              </li>
            </ul>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
@endsection

@section('content')
  <section class="content-header mt-2">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h3>Data Website Koperasi</h3>
        </div>
      </div>
    </div><!-- /.container-fluid -->
    <div class="alert alert-primary" role="alert">
      Penghapusan data pada menu ini hanya dapat dilakukan oleh Admin. Apabila ingin menghapus data silahkan hubungi <a href="https://wa.me/6282237256677" style="text-decoration: none" target="blank"><b>Admin.</b></a>
    </div>
  </section>

  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h5><b>Data Grup Koperasi</b></h5>
        </div>
        <div class="card-header">
          <a href="{{route('TambahDataWebsite')}}" class="btn btn-primary"><i class="far fa-plus-square"></i> Tambah Data</a>
          <div class="card-tools">
            <div class="input-group input-group-sm" style="width: 150px;">
              <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

              <div class="input-group-append">
                <button type="submit" class="btn btn-default">
                  <i class="fas fa-search"></i>
                </button>
              </div>
            </div>
          </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body table-responsive p-0">
          <table class="table table-hover text-nowrap">
            <thead>
              <tr>
                <th>No</th>
                <th>Nama Koperasi</th>
                <th>Visi</th>
                <th>Misi</th>
                <th>Manfaat</th>
                <th>Link Facebook</th>
                <th>Link Instagram</th>
                <th>Link Whatsapp</th>
                <th>AD ART</th>
                <th>Syarat Keanggotaan</th>
                <th>Struktur Pengurus</th>
                <th>Action</th>
              </tr>
            </thead>
            @php
                $id = 1;
            @endphp
            <tbody>
              @foreach ($data as $datas)
              <tr>
                <td><?php echo $id; $id++; ?></td>
                <td>{{ $datas->nama_koperasi }}</td>
                <td>{{ $datas->visi }}</td>
                <td>{{ $datas->misi }}</td>
                <td>{{ $datas->manfaat }}</td>
                <td>{{ $datas->link_facebook }}</td>
                <td>{{ $datas->link_instagram }}</td>
                <td>{{ $datas->link_whatsapp }}</td>
                <td>{{ $datas->ad_art }}</td>
                <td>{{ $datas->syarat_keanggotaan }}</td>
                <td>{{ $datas->struktur_pengurus }}</td>
                <td>
                  <div class="row">
                    <a href="#" class="btn btn-warning"><i class="fas fa-edit"></i></a>
                    <a href="#" class="btn btn-danger"><i class="fas fa-trash-alt"></i></a>
                  </div>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        <!-- /.card-body -->
        <div class="card-footer clearfix">
          <div class="pagination pagination-sm float-right">
            {{ $data->links() }}
          </div>
        </div>
      </div>
      <!-- /.card -->
    </div>
    <!-- /.col -->
  </div>

  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h5><b>Data Kegiatan Koperasi</b></h5>
        </div>
        <div class="card-header">
          <a href="{{route('TambahDataKegiatan')}}" class="btn btn-primary"><i class="far fa-plus-square"></i> Tambah Data</a>
          <div class="card-tools">
            <div class="input-group input-group-sm" style="width: 150px;">
              <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

              <div class="input-group-append">
                <button type="submit" class="btn btn-default">
                  <i class="fas fa-search"></i>
                </button>
              </div>
            </div>
          </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body table-responsive p-0">
          <table class="table table-hover text-nowrap">
            <thead>
              <tr>
                <th>No</th>
                <th>Pelaksana Kegiatan</th>
                <th>Nama Kegiatan</th>
                <th>Tempat Kegiatan</th>
                <th>Tanggal Kegiatan</th>
                <th>File Dokumen</th>
                <th>Action</th>
              </tr>
            </thead>
            @php
                $id = 1;
            @endphp
            <tbody>
              @foreach ($kegiatan as $kegiatans)
              <tr>
                <td><?php echo $id; $id++; ?></td>
                <td>Data 1</td>
                <td>Data 1</td>
                <td>Data 1</td>
                <td>Data 1</td>
                <td>Data 1</td>
                <td>
                  <div class="row">
                    <a href="#" class="btn btn-warning"><i class="fas fa-edit"></i></a>
                    <a href="#" class="btn btn-danger"><i class="fas fa-trash-alt"></i></a>
                  </div>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        <!-- /.card-body -->
        <div class="card-footer clearfix">
          <div class="pagination pagination-sm float-right">
            {{ $kegiatan->links() }}
          </div>
        </div>
      </div>
      <!-- /.card -->
    </div>
    <!-- /.col -->
  </div>
@endsection