@extends('Admin.Layout.Main')

@section('css')
  <link rel="stylesheet" href="{{asset('admin/plugins/summernote/summernote-bs4.min.css')}}">
@endsection

@section('head')
    Data Produk
@endsection

@section('sidebar')
    <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="" class="brand-link">
      <img src="{{asset('assets/images/koperasi.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">Admin</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item">
            <a href="{{route('AdminPage')}}" class="nav-link">
              <i class="nav-icon fas fa-home"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-globe"></i>
              <p>
                Website
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('Website')}}" class="nav-link ">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Koperasi DKUM</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="{{route('Produk')}}" class="nav-link">
              <i class="nav-icon fas fa-boxes"></i>
              <p>
                Produk
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-database"></i>
              <p>
                Master Data
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('DataWebsite')}}" class="nav-link active">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Data Website</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('DataAdmin')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Data Admin</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('DataKaryawan')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Data Karyawan</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('DataAnggota')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Data Anggota</p>
                </a>
              </li>
            </ul>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
@endsection

@section('content')
  <section class="content-header mt-2">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h3>Formulir Produk</h3>
        </div>
      </div>
    </div><!-- /.container-fluid -->
    <div class="alert alert-primary" role="alert">
      Jika tidak terdapat kategori produk yang sesuai, silahkan hubungi <a href="https://wa.me/6282237256677" style="text-decoration: none" target="blank"><b>admin.</b></a>
    </div>
  </section>

  <div class="card card-secondary">
    <!-- form start -->
    <form action="{{route('InsertProduk')}}" method="POST">
      @csrf
      <div class="card-body">
        <div class="form-group">
          <label for="form">Nama Produk</label>
          <input type="text" name="nama_produk" class="form-control" id="form" placeholder="misal: Beras 10 Kg">
        </div>
        <div class="form-group">
          <label for="form">Harga (Rp)</label>
          <input type="number" name="harga" class="form-control" id="form" placeholder="100000">
        </div>
        <div class="form-group">
          <label for="form">Jumlah</label>
          <input type="number" name="jumlah" class="form-control" id="form" placeholder="100">
        </div>
        <div class="form-group">
          <label for="form">Kategori Produk</label>
          <select name="kategoriproduk" id="" class="form-control">
            @foreach ($kategori as $kategori)
                <option value="{{$kategori->id_kategori_produk}}">{{$kategori->nama_kategori}}</option>
            @endforeach
          </select>
        </div>
        <div class="form-group">
          <label for="form">Deskripsi</label>
          <textarea name="deskripsi" id="" cols="30" rows="10" class="form-control"></textarea>
        </div>
      </div>
      <!-- /.card-body -->

      <div class="card-footer">
        <input type="submit" value="Simpan" class="btn btn-primary">
        <a href="{{route('Produk')}}" class="btn btn-secondary">Kembali</a>
      </div>
    </form>
  </div>
@endsection