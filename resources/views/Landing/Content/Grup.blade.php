@extends('Landing.Layout.DashboardLayout')

@section('header')
    Grup
@endsection

@section('navbar')
    <header class="">
        <nav class="navbar navbar-expand-lg" id="navbar">
        <div class="container">
            <a class="navbar-brand" href="/"><h2>AJI'S BISNIS DIGITAL</h2></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item scrollto">
                <a class="nav-link" href="#top">Home
                    <span class="sr-only">(current)</span>
                </a>
                </li>
                <li class="nav-item">
                <a class="nav-link scrollto active" href="#About">Group</a>
                </li>  
                <li class="nav-item">
                <a class="nav-link scrollto" href="#Civitas">Civitas</a>
                </li>                          
                <li class="nav-item">
                <a class="nav-link scrollto" href="#Grup">Grup</a>
                </li>
                <li class="nav-item">
                <a class="nav-link scrollto" href="#Contact">Contact</a>
                </li>
            </ul>
            </div>
        </div>
        </nav>
    </header>
@endsection

@section('content')
    <!-- Page Content -->
    <div class="page-heading header-text">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <img src="{{asset('assets/images/koperasi.png')}}" alt="Koperasi" width="100px" class="mb-3 mt-3">
              <h1>Koperasi Duta Kriya Usaha Mandiri</h1>
              <span><em>Grup Duta Kriya Mandiri</em></span>
            </div>
          </div>
        </div>
      </div>
  
      <div class="team-grup mt-2 mb-4">
        <div class="container">
          <div class="row text-center">
            <div class="col-md-12">
              <div class="section-heading">
                <h2><em>Ingin melihat usaha yang mana ?</em></h2>
                <p>Pilih salah satu usaha yang ingin Anda akses untuk dapat menikmati layanan yang ditawarkan oleh grup Grup Duta Kriya Mandiri.</p>
              </div>
            </div>
            @foreach ($data as $datas)
              <div class="col-md-4">
                <div class="card-body border border-primary rounded">
                    <img src="{{ asset('assets/images/koperasi.png')}}" alt="" class="mb-3 mt-1" width="100px">
                    <h5>Koperasi Duta Kriya Usaha Mandiri</h5>
                    <p>"Terwujudnya pelayanan yang optimal untuk peningkatan kesejahteraan anggota"</p>
                    <a href="/Dashboard" class="btn btn-primary mt-4">Kunjungi</a>
                </div>
              </div>
            @endforeach
          </div>
        </div>
      </div>
@endsection