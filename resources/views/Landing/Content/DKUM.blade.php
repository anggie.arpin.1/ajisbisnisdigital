@extends('Landing.Layout.DashboardLayout')

@section('header')
    DKUM
@endsection

@section('navbar')
    <header class="">
        <nav class="navbar navbar-expand-lg" id="navbar">
        <div class="container">
            <a class="navbar-brand" href="/"><h2>AJI'S BISNIS DIGITAL</h2></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item scrollto">
                <a class="nav-link" href="#top">Home
                    <span class="sr-only">(current)</span>
                </a>
                </li>
                <li class="nav-item">
                <a class="nav-link scrollto active" href="#About">Group</a>
                </li>  
                <li class="nav-item">
                <a class="nav-link scrollto" href="#Civitas">Civitas</a>
                </li>                          
                <li class="nav-item">
                <a class="nav-link scrollto" href="#Grup">Grup</a>
                </li>
                <li class="nav-item">
                <a class="nav-link scrollto" href="#Contact">Contact</a>
                </li>
            </ul>
            </div>
        </div>
        </nav>
    </header>
@endsection

@section('content')
    <!-- Page Content -->
    <div class="page-heading header-text">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <img src="{{asset('assets/images/koperasi.png')}}" alt="Koperasi" width="100px" class="mb-3 mt-3">
              <h1>Koperasi Duta Kriya Usaha Mandiri</h1>
              <span><em>Grup Duta Kriya Mandiri</em></span>
            </div>
          </div>
        </div>
      </div>
  
      <div class="more-info about-info">
        <div class="container">
          <div class="row">
            <div class="col-12">
              <div class="more-info-content">
                <div class="row">
                  <div class="col-6 align-self-center">
                    <div class="right-content mb-4">
                      <span>Tentang Kami</span>
                      <h3 class="mb-2"><b>Visi</b></h3>
                      <h6>"Terwujudnya pelayanan yang optimal untuk peningkatan kesejahteraan anggota"</h6>
                    </div>
                    <div class="right-content">
                      {{-- <span>Tentang Kami</span> --}}
                      <h3 class="mb-2"><b>Misi</b></h3>
                      <ul>
                        <li class="mb-3"><i class="fas fa-dot-circle"></i> Meningkatkan profesionalisme pengelola koperasi.</li>
                        <li class="mb-3"><i class="fas fa-dot-circle"></i> Meningkatkan mutu manajemen dan tata kelola yang transparan dan akuntabel.</li>
                        <li class="mb-3"><i class="fas fa-dot-circle"></i> Meningkatkan partisipasi aktif anggota sebagai pemilik koperasi.</li>
                        <li class="mb-3"><i class="fas fa-dot-circle"></i> Meningkatkan partisipasi aktif anggota sebagai pengguna jasa koperasi.</li>
                        <li class="mb-3"><i class="fas fa-dot-circle"></i> Melakukan kerjasama usaha yang saling menguntungkan dalam rangka pengembangan koperasi.</li>
                    </ul>
                    </div>
                  </div>
                  <div class="col-6">
                    <div class="left-image">
                      <img src="{{asset('assets/images/4.jpeg')}}" alt="">
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  
      <div class="team">
        <div class="container">
          <div class="row d-flex justify-content-center">
            <div class="col-md-12">
              <div class="section-heading">
                <h2>Struktur <em>Organisasi</em></h2>
                <span>Transparansi merupakan hal utama bagi kami</span>
              </div>
            </div>
            <div class="col-md-8">
              <div class="team-item">
                <img src="https://saragusti22.files.wordpress.com/2017/12/struktur-organisasi.jpg" alt="Struktur Organisasi" width="50px">
              </div>
            </div>
          </div>
        </div>
      </div>
  
      <div class="fun-facts">
        <div class="container">
          <div class="row">
            <div class="col-md-6">
              <div class="left-content">
                <h2><em>Bergabunglah</em> <br> Bersama kami</h2>
                <p>Pellentesque ultrices at turpis in vestibulum. Aenean pretium elit nec congue elementum. Nulla luctus laoreet porta. Maecenas at nisi tempus, porta metus vitae, faucibus augue. 
                <br><br>Fusce et venenatis ex. Quisque varius, velit quis dictum sagittis, odio velit molestie nunc, ut posuere ante tortor ut neque.</p>
                {{-- <a href="" class="filled-button">Daftar Sekarang</a> --}}
              </div>
            </div>
            <div class="col-md-6 align-self-center">
              <div class="row">
                <div class="col-md-6">
                  <div class="count-area-content">
                    <div class="count-digit">945</div>
                    <div class="count-title">Work Hours</div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="count-area-content">
                    <div class="count-digit">1280</div>
                    <div class="count-title">Great Reviews</div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="count-area-content">
                    <div class="count-digit">578</div>
                    <div class="count-title">Projects Done</div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="count-area-content">
                    <div class="count-digit">26</div>
                    <div class="count-title">Awards Won</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  
      <div class="testimonials">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="section-heading">
                <h2><em>Kegiatan</em> Kami</h2>
                <span>Seluruh Aktivitas yang telah kami laksanakan</span>
              </div>
            </div>
            <div class="col-md-12">
              <div class="owl-testimonials owl-carousel">
                
                <div class="documentation">
                  <div class="inner-content">
                    <img src="https://previews.123rf.com/images/hitmansnr/hitmansnr1402/hitmansnr140200031/26284231-peaceful-landscape-in-istana-ubud-bali-indonesia.jpg" alt="">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
@endsection