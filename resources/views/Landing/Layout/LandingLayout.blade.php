<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="TemplateMo">
    <link href="{{asset('https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900&display=swap')}}" rel="stylesheet">

    <title>ABD | Dashboard</title>

    <link href="{{asset('favicon.ico')}}" rel="shortcut icon"  type="image/x-icon">
    <link rel="stylesheet" href="{{asset('admin/plugins/fontawesome-free/css/all.min.css')}}">  

    <!-- Bootstrap core CSS -->
    <link href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Additional CSS Files -->
    <link rel="stylesheet" href="{{asset('assets/css/fontawesome.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/templatemo-finance-business.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/owl.css')}}">
  </head>

  <body>
    <!-- ***** Preloader Start ***** -->
    <div id="preloader">
        <div class="jumper">
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>  
    <!-- ***** Preloader End ***** -->

    <header class="">
      <nav class="navbar navbar-expand-lg" id="navbar">
        <div class="container">
          <a class="navbar-brand" href="/"><h2>AJI'S BISNIS DIGITAL</h2></a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
              <li class="nav-item scrollto">
                <a class="nav-link" href="/">Home
                  <span class="sr-only">(current)</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link scrollto" href="{{route('grup')}}">Group</a>
              </li>  
              {{-- <li class="nav-item">
                <a class="nav-link scrollto" href="#Civitas">Civitas</a>
              </li>                           --}}
              <li class="nav-item">
                <a class="nav-link scrollto" href="/Marketplace">Marketplace</a>
              </li>
              {{-- <li class="nav-item">
                <a class="nav-link scrollto" href="#Contact">Contact</a>
              </li> --}}
              <li class="nav-item">
                <a href="/Login" class="filled-button">Masuk</a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </header>

    <!-- Page Content -->
    <!-- Banner Starts Here -->
    <div class="main-banner header-text" id="top">
        <div class="Modern-Slider">
          <!-- Item -->
          <div class="item item-3">
            <div class="img-fill">
                <div class="text-content">
                  <div class="row">
                    <div class="col-md-4">
                      <img src="https://scontent.fsub6-7.fna.fbcdn.net/v/t39.30808-6/257211411_2779972468815868_6666387441023960639_n.jpg?_nc_cat=103&ccb=1-5&_nc_sid=09cbfe&_nc_eui2=AeHgKyFDYuCT8I9SRG9LPyq8Q569gcO36DFDnr2Bw7foMWwMhMzvR4JXRnWPuhR_iTDWchdE-2RL2jHb7tUBLQz1&_nc_ohc=3Sb_fsb_W8cAX9JDUZN&_nc_zt=23&_nc_ht=scontent.fsub6-7.fna&oh=00_AT-eX47tgmWCpngOxCIAz7t3jSlVbOBrlevE9BqNxxLaHA&oe=61BC1D41" class="image-fluid rounded-circle" alt="" width="150px">
                    </div>
                    <div class="col-md-6 mt-4 ms-3">
                      <h4>AJI'S BISNIS DIGITAL</h4>
                      <h6>Group Duta Kriya Mandiri</h6>
                      <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Laboriosam est esse impedit sed at veritatis, possimus excepturi quam harum consequatur, cumque repellat! Sapiente, qui quod. Temporibus earum iste architecto iusto.</p>
                    </div>
                  </div>
                </div>
            </div>
          </div>
          <!-- // Item -->
        </div>
    </div>
    <!-- Banner Ends Here -->

    <div class="more-info">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="more-info-content">
              <div class="row">
                <div class="col-md-6">
                  <div class="left-image">
                    <img src="{{asset('assets/images/1.jpeg')}}" alt="">
                  </div>
                </div>
                <div class="col-md-6 align-self-center">
                  <div class="right-content">
                    <span>Tentang Kami</span>
                    <h2>Apa Itu <em>Aji's Bisnis Digital</em></h2>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quam voluptas consequuntur autem possimus odio esse assumenda vel nemo quod ea tempore error quae corporis iusto quidem, excepturi labore voluptates magni quisquam ad? Et illo officia beatae sint dolorem, a vel!</p>
                    {{-- <a href="#" class="filled-button">Read More</a> --}}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="fun-facts-landing" id="Civitas">
      <div class="container text-center">
        <iframe width="852" height="480" src="https://www.youtube.com/embed/G343-rbdEcA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </div>
    </div>

    <div class="services" id="Grup">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="section-heading">
              <h2>Mengapa <em>Aji's Bisnis Digital</em></h2>
              <span>Kami memberikan banyak keuntungan kepada anda yang ingin bergabung bersama kami</span>
            </div>
          </div>
          <div class="col-md-4">
            <div class="service-item">
              <div class="down-content text-center">
                <img src="https://cdn-icons.flaticon.com/png/512/696/premium/696755.png?token=exp=1639281046~hmac=11cac5d85628209959899ce6cda9e6a4" alt="image" width="20px">
                <h4>Digital Currency</h4>
                <p>Sed tincidunt dictum lobortis. Aenean tempus diam vel augue luctus dignissim. Nunc ornare leo tortor.</p>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="service-item">
              <div class="down-content text-center">
                <img src="https://cdn-icons.flaticon.com/png/512/696/premium/696755.png?token=exp=1639281046~hmac=11cac5d85628209959899ce6cda9e6a4" alt="image" width="20px">
                <h4>Market Analysis</h4>
                <p>Sed tincidunt dictum lobortis. Aenean tempus diam vel augue luctus dignissim. Nunc ornare leo tortor.</p>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="service-item">
              <div class="down-content text-center">
                <img src="https://cdn-icons.flaticon.com/png/512/696/premium/696755.png?token=exp=1639281046~hmac=11cac5d85628209959899ce6cda9e6a4" alt="image" width="20px">
                <h4>Historical Data</h4>
                <p>Sed tincidunt dictum lobortis. Aenean tempus diam vel augue luctus dignissim. Nunc ornare leo tortor.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="testimonials-landing">
      <div class="container">
        <div class="row d-flex justify-content-center">
          <div class="col-md-12 mt-2">
            <div class="section-heading">
              <h2>Group</h2>
              <span><em>Duta Kriya Mandiri</em></span>
            </div>
          </div>
          {{-- <div class="col-md-12">
            <div class="owl-testimonials owl-carousel">
              <div class="testimonial-item">
                <div class="inner-content">
                  <h4>Andrew Boom</h4>
                  <span>Marketing Head</span>
                  <p>"Curabitur sollicitudin, tortor at suscipit volutpat, nisi arcu aliquet dui, vitae semper sem turpis quis libero. Quisque vulputate lacinia nisl ac lobortis."</p>
                </div>
                <img src="http://placehold.it/60x60" alt="">
              </div>
            </div>
          </div> --}}
          <div class="col-md-4 text-center">
              <div class="testimonial-item">
                <div class="inner-content">
                  <img src="{{ asset('assets/images/koperasi.png')}}" alt="" class="mb-3 mt-1">
                  <h4>Koperasi Duta Kriya Usaha Mandiri</h4>
                  <span>Unit Usaha</span>
                  <p>"Terwujudnya pelayanan yang optimal untuk peningkatan kesejahteraan anggota"</p>
                  <a href="/Dashboard" class="filled-button mt-4">Kunjungi</a>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    @include('Landing.Layout.footer')

    <!-- Bootstrap core JavaScript -->
    <script src="{{asset('vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

    <!-- Additional Scripts -->
    <script src="{{asset('assets/js/custom.js')}}"></script>
    <script src="{{asset('assets/js/owl.js')}}"></script>
    <script src="{{asset('assets/js/slick.js')}}"></script>
    <script src="{{asset('assets/js/accordions.js')}}"></script>
    <script src="{{asset('assets/js/app.js')}}"></script>

    <script language = "text/Javascript"> 
      cleared[0] = cleared[1] = cleared[2] = 0; //set a cleared flag for each field
      function clearField(t){                   //declaring the array outside of the
      if(! cleared[t.id]){                      // function makes it static and global
          cleared[t.id] = 1;  // you could use true and false, but that's more typing
          t.value='';         // with more chance of typos
          t.style.color='#fff';
          }
      }
    </script>

  </body>
</html>