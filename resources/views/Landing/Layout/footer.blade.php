<!-- Footer Starts Here -->
<footer id="Contact">
    <div class="container">
      <div class="row">
        <div class="col-md-3 footer-item">
          <h4>AJI'S BISNIS DIGITAL</h4>
          <p>Banjar Kanginan, Desa Julah<br>
            Buleleng, Bali <br>
            Kode Pos 81173</p>
          <ul class="social-icons">
            <li><a href="https://web.facebook.com/Koperasi-Duta-Kriya-Usaha-Mandiri-109315978203008" target="_blank"><i class="fa fa-facebook"></i></a></li>
            <li><a href="https://www.instagram.com/koperasi_dkum/" target="blank"><i class="fa fa-instagram"></i></a></li>
            <li><a href="https://wa.me/6283114352175" target="blank"><i class="fa fa-whatsapp"></i></a></li>
          </ul>
        </div>
        <div class="col-md-3 footer-item">
          <h4>Halaman Lain</h4>
          <ul class="menu-list">
            <li><a href="#">Marketplace</a></li>
            <li><a href="#">AD & ART</a></li>
            <li><a href="{{route('grup')}}">Grup Kami</a></li>
            <li><a href="#">Struktur Organisasi</a></li>
          </ul>
        </div>
        <div class="col-md-3 footer-item">
          <h4>Tentang Kami</h4>
          <ul class="menu-list">
            <li><a href="#">Kebijakan Privasi</a></li>
            <li><a href="#">Profil Pengembang</a></li>
            <li><a href="#">Informasi</a></li>
            <li><a href="#">Syarat Pendaftaran Anggota</a></li>
          </ul>
        </div>
        <div class="col-md-3 footer-item last-item">
          {{-- <h4>Contact Us</h4>
          <div class="contact-form">
            <form id="contact footer-contact" action="" method="post">
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <fieldset>
                    <input name="name" type="text" class="form-control" id="name" placeholder="Full Name" required="">
                  </fieldset>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <fieldset>
                    <input name="email" type="text" class="form-control" id="email" pattern="[^ @]*@[^ @]*" placeholder="E-Mail Address" required="">
                  </fieldset>
                </div>
                <div class="col-lg-12">
                  <fieldset>
                    <textarea name="message" rows="6" class="form-control" id="message" placeholder="Your Message" required=""></textarea>
                  </fieldset>
                </div>
                <div class="col-lg-12">
                  <fieldset>
                    <button type="submit" id="form-submit" class="filled-button">Send Message</button>
                  </fieldset>
                </div>
              </div>
            </form>
          </div> --}}
          <h4>Peta Lokasi</h4>
          <div class="contact-form">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3950.2628629607293!2d115.19922631466609!3d-8.07465048294496!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd196ba09c79329%3A0x310acf14bf57115d!2sJl.%20Raya%20Air%20Sanih%2C%20Kabupaten%20Buleleng%2C%20Bali!5e0!3m2!1sid!2sid!4v1638702668587!5m2!1sid!2sid" width="400" height="180" style="border:0;" allowfullscreen="" loading="lazy"></iframe> 
          </div>
        </div>
      </div>
    </div>
  </footer>
  
  <div class="sub-footer">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <p>Copyright &copy; 2021 Aji's Bisnis Digital
        </div>
      </div>
    </div>
  </div>